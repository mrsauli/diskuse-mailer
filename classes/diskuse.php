<?php
class DiskUse {
    private $messages = [];
    private $command = "df";
    private $site = '';
    private $total, $inuse, $free, $percentageuse;
    private $threshold = 1048576;
    private $diff = 0;
    private $alertMessage = "";
    

    public function __construct($site) {
        $this->site = $site;

        $output = explode(PHP_EOL, shell_exec($this->command));
        $dfline = preg_split("/\s+/", $output[1]);
        [, $total, $inuse, $free, $percentageuse,] = $dfline;

        $this->total = $total;
        $this->inuse = $inuse;
        $this->free = $free;
        $this->percentageuse = $percentageuse;

        $this->messages = [
            sprintf("On %s there's a total diskspace of %s, of which %s (%s) is in use.", $this->site, $this->toGb($total), $this->toGb($inuse), $percentageuse),
            sprintf("Hey! Just wanted to let you know about the state of the disk of %s. There's %s total, of which %s (%s) is in use.", $site, $this->toGb($total), $this->toGb($inuse), $percentageuse),
            sprintf("Good Morning! Of %s total on %s, %s (%s) is in use.", $this->toGb($total),$this->site,$this->toGb($inuse),$percentageuse),
            sprintf("😎💾 %s h %s free (%s in use) of %s diskspace",$this->site, $this->toGb($free), $percentageuse, $this->toGb($total))
        ];
    }

    public function getMessage() {
        return trim(sprintf("%s %s",$this->messages[array_rand($this->messages)], $this->alertMessage));
    }

    public function toGb(int $number) :string {
        return sprintf("%.1f Gb", $number/1048576); // this assumes 1k blocks
    }

    public function getSpace() {
        // die(var_dump( intval($this->total),intval($this->inuse)  ));
        return [
            "total" => intval($this->total),
            "used" =>intval($this->inuse)
        ];
    }

    public function setDiff($diff) {
        $this->diff = $diff;
        if ($diff > $this->threshold) {
            $this->alertMessage = sprintf("We're up +%s from yesterday.", $this->toGb($diff));
        }
    }  
}
